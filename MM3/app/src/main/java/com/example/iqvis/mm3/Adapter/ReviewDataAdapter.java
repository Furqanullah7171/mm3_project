package com.example.iqvis.mm3.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.R;
import com.example.iqvis.mm3.ViewHolders.BusinessViewHolder;
import com.example.iqvis.mm3.ViewHolders.ReviewViewHolder;

import java.util.ArrayList;

/**
 * Created by IQVIS on 8/27/2017.
 */

public class ReviewDataAdapter extends RecyclerView.Adapter<ReviewViewHolder> {
   Context context;
    ArrayList<DataResponse.Review> list;

    public ReviewDataAdapter(Context context, ArrayList<DataResponse.Review> list){
       this.list=list;
        this.context=context;
   }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View v= LayoutInflater.from(context).inflate(R.layout.review_list_snippet,parent,false);

        return new ReviewViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {

        holder.UpdateUI(list.get(position),context);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

package com.example.iqvis.mm3.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iqvis.mm3.Activities.MainActivity;
import com.example.iqvis.mm3.Adapter.BusinessDataAdapter;
import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.DataServices.BusinessDataService;
import com.example.iqvis.mm3.Interfaces.OnItemClick;
import com.example.iqvis.mm3.Interfaces.onBusinessDataRecieved;
import com.example.iqvis.mm3.R;

import java.util.ArrayList;

/**
 * Created by IQVIS on 8/27/2017.
 */

public class BusinessFragment extends Fragment implements onBusinessDataRecieved,OnItemClick {
  RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.business_fragment,container,false);
        recyclerView =(RecyclerView) v.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(false);
        BusinessDataService.getInstance().GetBusiness(getContext(),"1","30.663670","73.099197","1000",this);


        return v;
    }

    @Override
    public void onSuccess(DataResponse data) {

        BusinessDataAdapter adapter =  new BusinessDataAdapter(getActivity(), (ArrayList<DataResponse.Business>) data.getBusiness(),this);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onFailed(String error) {
        Toast.makeText(getActivity(), "Failed to draw Recycler View", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(DataResponse.Business obj) {
        ((MainActivity)getActivity()).currentObj = obj;

        getFragmentManager().beginTransaction().replace(R.id.fragment_container,new BusinessDetailsFragment()).commit();
    }
}

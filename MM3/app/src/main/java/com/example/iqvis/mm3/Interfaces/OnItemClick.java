package com.example.iqvis.mm3.Interfaces;

import com.example.iqvis.mm3.DataObjects.DataResponse;

/**
 * Created by IQVIS on 8/27/2017.
 */

public interface OnItemClick {
void onClick(DataResponse.Business obj);
}

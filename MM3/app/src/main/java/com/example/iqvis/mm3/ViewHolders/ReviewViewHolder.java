package com.example.iqvis.mm3.ViewHolders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by IQVIS on 8/27/2017.
 */

public class ReviewViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView name, desc;
    RatingBar bar;

    public ReviewViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.user_dp);
        name = (TextView) itemView.findViewById(R.id.user_name);
        desc = (TextView) itemView.findViewById(R.id.user_comments);
        bar = (RatingBar) itemView.findViewById(R.id.reviewBar);
    }

    public void UpdateUI(DataResponse.Review obj, Context context) {
        Picasso.with(context).load(obj.getImage()).into(imageView);
        name.setText(obj.getFname() + " " + obj.getLname());
        desc.setText(obj.getReview());
        bar.setRating(Float.parseFloat(obj.getRating()));
    }
}

package com.example.iqvis.mm3.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.iqvis.mm3.Activities.MainActivity;
import com.example.iqvis.mm3.Adapter.ReviewDataAdapter;
import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by IQVIS on 8/27/2017.
 */

public class BusinessDetailsFragment extends BusinessFragment {
RecyclerView recyclerView ;
    DataResponse.Business obj;
    ImageView bImage;
    TextView bname,bdesc;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.business_details_fragment,container,false);
obj = ((MainActivity)getActivity()).currentObj;
        recyclerView = (RecyclerView)v.findViewById(R.id.reviewRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ReviewDataAdapter adapter =  new ReviewDataAdapter(getActivity(), (ArrayList<DataResponse.Review>) obj.getReview());
     recyclerView.setAdapter(adapter);
        bImage = (ImageView)v.findViewById(R.id.bImage);
        bname =(TextView)v.findViewById(R.id.bName);
        bdesc =(TextView)v.findViewById(R.id.bDesc);
        Picasso.with(getActivity()).load(obj.getBimage()).into(bImage);
        bname.setText(obj.getBname());
        bdesc.setText(obj.getBdescription());
        return  v;
    }
}
package com.example.iqvis.mm3.DataServices;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.Interfaces.onBusinessDataRecieved;
import com.example.iqvis.mm3.Managers.SharedPrefManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by IQVIS on 8/27/2017.
 */

public class BusinessDataService {
    private static final BusinessDataService ourInstance = new BusinessDataService();

    public static BusinessDataService getInstance() {
        return ourInstance;
    }

    private BusinessDataService() {
    }

    public void GetBusiness(final Context context, String id, String lat, String lon, String range, final onBusinessDataRecieved callback){

        HashMap<String,String> map=  new HashMap<>();
        map.put("id",id);
        map.put("lat",lat);
        map.put("lon",lon);
        map.put("range",range);

        String url = "https://kolaverid.herokuapp.com/getBusiness";
        JsonObjectRequest request =  new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject res) {
//TODO Parsing Data from JSON
                try {
                    Gson gson = new GsonBuilder().create();
                    DataResponse dataResponse = gson.fromJson(res.toString(),DataResponse.class);
                    callback.onSuccess(dataResponse);

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              callback.onFailed(error.getMessage());
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(context).add(request);
    }

}

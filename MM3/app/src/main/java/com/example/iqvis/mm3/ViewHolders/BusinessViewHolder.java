package com.example.iqvis.mm3.ViewHolders;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.Interfaces.OnItemClick;
import com.example.iqvis.mm3.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by IQVIS on 8/27/2017.
 */

public class BusinessViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView name, desc;
    RatingBar bar;

    public BusinessViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.business_image);
        name = (TextView) itemView.findViewById(R.id.business_name);
        desc = (TextView) itemView.findViewById(R.id.business_description);
        bar = (RatingBar) itemView.findViewById(R.id.ratingBar);
    }

    public void UpdateUI(final DataResponse.Business obj, Context context, final OnItemClick callback) {

        Picasso.with(context).load(obj.getBimage()).into(imageView);
        name.setText(obj.getBname());
        desc.setText(obj.getBdescription());
        ArrayList<DataResponse.Review> arList = (ArrayList<DataResponse.Review>) obj.getReview();
        float ratingTotal = 0.0f;
        for (DataResponse.Review r : arList) {
            ratingTotal += Float.parseFloat(r.getRating());
        }
        ratingTotal = ratingTotal / arList.size();
        bar.setRating(ratingTotal);
        itemView.findViewById(R.id.snippet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(obj);
            }
        });


    }
}

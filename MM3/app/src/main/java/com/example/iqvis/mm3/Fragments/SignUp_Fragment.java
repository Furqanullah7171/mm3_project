package com.example.iqvis.mm3.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.iqvis.mm3.DataServices.AccountDataService;
import com.example.iqvis.mm3.R;

/**
 * Created by IQVIS on 8/20/2017.
 */

public class SignUp_Fragment extends Fragment {
    EditText email,password,name;
    Button submit;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.signup_fragment,container,false);

        email = (EditText)v.findViewById(R.id.user_email_edit_text_sign_up);
        password = (EditText)v.findViewById(R.id.user_password_edit_text_sign_up);
        name = (EditText)v.findViewById(R.id.et_name);

        submit = (Button)v.findViewById(R.id.sign_up_login_btn_);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String u,p,n;
                u= email.getText().toString().trim();
                p = password.getText().toString().trim();
                n = name.getText().toString().trim();
             String [] names;
                if(n.contains(" ")){
                   names =  n.split(" ");
                }else{
                    names =  new String[1];
                    names[0] = n;
                }

                AccountDataService.getInstance().SignUpRequest(getActivity(),u,p,names);
            }
        });
        return v;
    }
}

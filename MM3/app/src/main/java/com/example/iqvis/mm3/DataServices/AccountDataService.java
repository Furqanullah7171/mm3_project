package com.example.iqvis.mm3.DataServices;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.example.iqvis.mm3.Managers.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by IQVIS on 8/26/2017.
 */

public class AccountDataService {
    private static final AccountDataService ourInstance = new AccountDataService();

    public static AccountDataService getInstance() {
        return ourInstance;
    }

    private AccountDataService() {
    }

public void LoginRequest(final Context context,String email,String pass){

    HashMap<String,String> map=  new HashMap<>();
    map.put("email",email);
    map.put("pass",pass);

String url = "https://kolaverid.herokuapp.com/SignIn";
JsonObjectRequest request =  new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map), new Response.Listener<JSONObject>() {
    @Override
    public void onResponse(JSONObject res) {
//TODO Parsing Data from JSON
        try {
            JSONObject response = res.getJSONObject("response");
            String code =  response.getString("code");
            String message =  response.getString("message");
            JSONObject user =  res.getJSONObject("user");
            String fname = user.getString("fname") ;
            String lname = user.getString("lname") ;
            String email = user.getString("email") ;
            String dob = user.getString("dob") ;
            SharedPrefManager.getInstance(context).setUser(fname+" "+lname,email,dob);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}, new Response.ErrorListener() {
    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
    }
});
    Volley.newRequestQueue(context).add(request);
}

    public void SignUpRequest(final Context context,String email,String pass,String [] names){

        HashMap<String,String> map=  new HashMap<>();
        map.put("email",email);
        map.put("pass",pass);
        if(names.length>1){
            map.put("fname",names[0]);
            map.put("lname",names[1]);
        }else{
            map.put("fname",names[0]);
            map.put("lname","");
        }
        String url = "https://kolaverid.herokuapp.com/AddUser";
        JsonObjectRequest request =  new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject res) {
//TODO Parsing Data from JSON
                try {
                    JSONObject response = res.getJSONObject("response");
                    String code =  response.getString("code");
                    String message =  response.getString("message");
                    if(code.equals("200")){
                        Toast.makeText(context, "Added", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context, "Not Added", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(context).add(request);
    }



}

package com.example.iqvis.mm3.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.Interfaces.OnItemClick;
import com.example.iqvis.mm3.R;
import com.example.iqvis.mm3.ViewHolders.BusinessViewHolder;

import java.util.ArrayList;

/**
 * Created by IQVIS on 8/27/2017.
 */

public class BusinessDataAdapter extends RecyclerView.Adapter<BusinessViewHolder> {
   Context context;
    ArrayList<DataResponse.Business> list;
   OnItemClick calbak;
    public  BusinessDataAdapter(Context context, ArrayList<DataResponse.Business> list,OnItemClick calbak){
       this.list=list;
        this.context=context;
   this.calbak=calbak;

    }

    @Override
    public BusinessViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View v= LayoutInflater.from(context).inflate(R.layout.business_list_snippet,parent,false);

        return new BusinessViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BusinessViewHolder holder, int position) {

        holder.UpdateUI(list.get(position),context,calbak);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

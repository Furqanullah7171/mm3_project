package com.example.iqvis.mm3.Managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

/**
 * Created by IQVIS on 8/26/2017.
 */

public class SharedPrefManager {
    private static SharedPrefManager ourInstance ;
    public static SharedPrefManager getInstance(Context context) {
        if(ourInstance==null){
            ourInstance =  new SharedPrefManager(context);
        }
        return ourInstance;
    }
    SharedPreferences pref;
    private SharedPrefManager(Context context) {
        pref =  context.getSharedPreferences("LOCAL",Context.MODE_PRIVATE);
    }


   public  void setUser(String name,String email,String dob){
        SharedPreferences.Editor editor =  pref.edit();
        editor.putString("name",name);
        editor.putString("email",email);
        editor.putString("dob",dob);
        editor.apply();
    }

   public Bundle getUser(){
       Bundle  b =  new Bundle();
       String name = pref.getString("name","");
       b.putString("name",name);
       b.putString("email",pref.getString("email",""));
       b.putString("dob",pref.getString("dob",""));
       return b;
   }

}

package com.example.iqvis.mm3.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.iqvis.mm3.DataObjects.DataResponse;
import com.example.iqvis.mm3.Fragments.BusinessFragment;
import com.example.iqvis.mm3.Fragments.FragmentMain;
import com.example.iqvis.mm3.Fragments.Login_Fragment;
import com.example.iqvis.mm3.R;

public class MainActivity extends AppCompatActivity {

    public DataResponse.Business currentObj = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
getSupportActionBar().hide();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,new BusinessFragment()).commit();
    }
}

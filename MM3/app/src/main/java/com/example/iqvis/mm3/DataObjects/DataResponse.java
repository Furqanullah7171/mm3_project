package com.example.iqvis.mm3.DataObjects;

/**
 * Created by IQVIS on 8/27/2017.
 */

        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;
public class DataResponse {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("business")
    @Expose
    private List<Business> business = null;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public List<Business> getBusiness() {
        return business;
    }

    public void setBusiness(List<Business> business) {
        this.business = business;
    }




    public class Business {

        @SerializedName("bid")
        @Expose
        private Integer bid;
        @SerializedName("bname")
        @Expose
        private String bname;
        @SerializedName("blocation")
        @Expose
        private String blocation;
        @SerializedName("bdescription")
        @Expose
        private String bdescription;
        @SerializedName("bimage")
        @Expose
        private String bimage;
        @SerializedName("bcid")
        @Expose
        private Integer bcid;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("contact")
        @Expose
        private String contact;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lan")
        @Expose
        private String lan;
        @SerializedName("ditance")
        @Expose
        private Double ditance;
        @SerializedName("review")
        @Expose
        private List<Review> review = null;

        public Integer getBid() {
            return bid;
        }

        public void setBid(Integer bid) {
            this.bid = bid;
        }

        public String getBname() {
            return bname;
        }

        public void setBname(String bname) {
            this.bname = bname;
        }

        public String getBlocation() {
            return blocation;
        }

        public void setBlocation(String blocation) {
            this.blocation = blocation;
        }

        public String getBdescription() {
            return bdescription;
        }

        public void setBdescription(String bdescription) {
            this.bdescription = bdescription;
        }

        public String getBimage() {
            return bimage;
        }

        public void setBimage(String bimage) {
            this.bimage = bimage;
        }

        public Integer getBcid() {
            return bcid;
        }

        public void setBcid(Integer bcid) {
            this.bcid = bcid;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLan() {
            return lan;
        }

        public void setLan(String lan) {
            this.lan = lan;
        }

        public Double getDitance() {
            return ditance;
        }

        public void setDitance(Double ditance) {
            this.ditance = ditance;
        }

        public List<Review> getReview() {
            return review;
        }

        public void setReview(List<Review> review) {
            this.review = review;
        }

    }
    public class Response {

        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("message")
        @Expose
        private String message;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }
    public class Review {

        @SerializedName("date_created")
        @Expose
        private String dateCreated;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("p_rating")
        @Expose
        private Integer pRating;
        @SerializedName("review")
        @Expose
        private String review;
        @SerializedName("uid")
        @Expose
        private String uid;
        @SerializedName("fname")
        @Expose
        private String fname;
        @SerializedName("lname")
        @Expose
        private String lname;
        @SerializedName("image")
        @Expose
        private String image;

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public Integer getPRating() {
            return pRating;
        }

        public void setPRating(Integer pRating) {
            this.pRating = pRating;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }
}


